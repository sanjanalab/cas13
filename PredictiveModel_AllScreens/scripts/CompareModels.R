#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressWarnings(suppressMessages(library(reshape2, quietly = T)))
suppressWarnings(suppressMessages(library(ggplot2, quietly = T)))

###### sub-routines #################################

########################## sub-routines end  ########

###### execute ######################################


# load data
Minimal = read.csv("./data/Learning_approaches_Spearman_RFgfp.csv")
Combined = read.csv("./data/Learning_approaches_Spearman_v3.csv")

# get Random Forest
GetMethod = function(x,col = "RandomForest"){x[,col]}

RF = list()
RF[[1]] = GetMethod(Minimal)
RF[[2]] = GetMethod(Combined)
names(RF) = c("RFgfp", "RFcombined")

df = melt(RF)
colnames(df) = c("Spearman", "Model")
df$Model = factor(df$Model , levels = names(RF) )

medians = aggregate(Spearman ~  Model, df, median)

pdf("./figures/Fig3b_Model_comparison.pdf", width = 4, height = 3, useDingbats = F)
ggplot(data = df, aes( x = Model , y = Spearman)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Model)) +
  scale_fill_manual( values = c( "#4292c6" , "#08519c")) +
  theme_classic() +
  ylab("spearmean coef. to held-out data") +
  xlab("") +
  #ylim(c(-0.12,0.72))+
  #theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  coord_fixed(ratio=15) + 
  geom_text(data = medians, aes(label = round(medians$Spearman,2), y = medians$Spearman + 0.025), colour = c("black",'black'), size=2.5) + 
  theme(axis.title.x=element_blank(),axis.text.x=element_blank(),axis.ticks.x = element_blank(), axis.line.x = element_blank())
dev.off()
