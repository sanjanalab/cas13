#!/bin/bash
#
#$ -N GPred
#$ -t 1-N
#$ -tc 50
#$ -e /dev/null
#$ -o /dev/null
#$ -cwd
#$ -V

module purge
module load R/3.5.1

echo "Current working directory: $(pwd)"
echo "Current task: $SGE_TASK_ID"
echo "Started at $(date)"

FASTA=($(pwd)/*.fasta)


fasta="${FASTA[$SGE_TASK_ID - 1]}"
SAMPLE_NAME=$(basename -s .fasta $fasta)


####################### Cas13design ######################


echo "Predicting guides for $SAMPLE_NAME $fasta"

SCRIPT="../../Cas13designGuidePredictor/scripts/RfxCas13d_GuideScoring.R"
MODELINPUT="../../Cas13designGuidePredictor/data/Cas13designGuidePredictorInput.csv"


# Predict guides
Rscript ${SCRIPT} ${fasta} ${MODELINPUT} false
