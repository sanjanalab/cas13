#!/bin/bash

DIRs=$(ls ./data/ | grep -E "ENST00000")


for dir in $DIRs;do 
 N=$(ls ./data/${dir}/*.fasta | wc -l)
 less ./scripts/Array.sh | sed "s/#$ -t 1-N/#$ -t 1-$N/" > ./data/${dir}/Array.sh
 cd ./data/${dir}/
 qsub Array.sh
 cd ../..
done 
