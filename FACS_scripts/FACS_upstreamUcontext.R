#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggridges, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))

Calculate_gMFI=function(a){
  m = abs(min(a))+1
  return(geoMean(a+m))}

###### execute ######################################
# load data
data = read.delim( file = "./FACS_data/FACS_upstreamUcontext.csv", sep=",",  header = T,  stringsAsFactors = F)


#Plot histograms
df = melt(data)
df$variable = as.character(df$variable)
df$Replicate = sapply(as.character(df$variable) , FUN = function(x){tmp = strsplit(x, split = "\\.")[[1]] 
                                                                tmp[length(tmp)]})
df$Guide = sapply(as.character(df$variable) , FUN = function(x){tmp = strsplit(x, split = "\\.")[[1]] 
                                                                tmp = tmp[-length(tmp)]
                                                                strsplit(tmp, split = "_")[[1]][2]})
df$Guide[is.na(df$Guide)] = "Lipo"
df$Plasmid = sapply(as.character(df$variable) , FUN = function(x){tmp = strsplit(x, split = "\\.")[[1]] 
                                                                tmp = tmp[-length(tmp)]
                                                                strsplit(tmp, split = "_")[[1]][1]})
df$PosR = sapply(as.character(df$variable) , FUN = function(x){tmp = strsplit(x, split = "\\.")[[1]] 
                                                                paste0(tmp[-length(tmp)], collapse = "_") })



df$variable = factor(df$variable , levels = names(table(df$variable)))
df$PosR = factor(df$PosR, levels = names(table(df$PosR)))
df$Guide = factor(df$Guide, levels = c("G81" , "NT", "Lipo" ))

# Plot histograms

gg1 = ggplot(df, aes(x = value+1, y = variable)) +
  # facet_wrap(~FullName, ncol = 3) +
  geom_density_ridges(aes(fill = Guide)) +
  scale_x_log10(limits=c(10,10000)) +
  theme_classic() +
  ggtitle("") +
  xlab("GFP signal") +
  ylab("Guide") + 
  scale_fill_manual(values = c("#de2d26","#bdbdbd","#f0f0f0"))


gg2 = ggplot(df, aes(x = value+1, y = PosR)) +
  # facet_wrap(~FullName, ncol = 3) +
  geom_density_ridges(aes(fill = Guide)) +
  scale_x_log10(limits=c(10,10000)) +
  theme_classic() +
  ggtitle("") +
  xlab("GFP signal") +
  ylab("Guide") + 
  scale_fill_manual(values = c("#de2d26","#bdbdbd","#f0f0f0"))

lay <- rbind(c(2,1),
             c(2,NA))

# pdf("./FACS_figures/upstreamUcontext_Histograms.pdf", width = 6, height = 12, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= list(gg1,gg2) ,layout_matrix = lay))
# dev.off()















# Calculate Mean MFI per sample
MFIs = colMeans(data) # colmeans to get average mfi per column
# MFIs = apply(data, MARGIN = 2, Calculate_gMFI )

# calculate % knockdown based on MFI per ortholog, per guide and per replicate
pLCRs = c("p130","p131","p132","p133","p134","p135","p136","p137","p138") # list of vector/sample names
Replicates = c(1,2,3) # replicate numbers
Guides = c("G81") # target guide names
Reference = "NT" # reference guide name

#initialize output vector
output.top.percent = vector()
#initialize counter
counter = 0


# loop through pLCRs vector names
for ( i in 1:length(pLCRs) ){
  
  pLCR = pLCRs[i] # select a pLCR name one after the other dependent on i
  
  pLCR_MFI = MFIs[grep(pLCR, names(MFIs))] # grep all MFI values for that particular pLCR name
  
  # loop through the number of replicates
  for (k in 1:length(Replicates)){
    
    rep = paste0("\\.",Replicates[k]) # Get current replicate number
    
    pLCR_MFI_rep = pLCR_MFI[grep(rep, names(pLCR_MFI))] # grep all MFI values for that particular pLCR name and a replicate
    
    if (length(pLCR_MFI_rep) == 2){
      # # loop through the number of Guides
      for (j in 1:length(Guides)){
        
        counter = counter + 1 #increase counter to allow storage in target vector
        
        guide = Guides[j] # get current guide name 
        
        guide.top = pLCR_MFI_rep[ grep(guide, names(pLCR_MFI_rep))] # get MFI of current guide 
        
        ref.top = pLCR_MFI_rep[ grep(Reference, names(pLCR_MFI_rep))]  # get MFI of current reference 
        
        output.top.percent[counter] <- round(guide.top/ref.top * 100, 2) # get MFI ratio of guide/ref and transform to percent and store output
        
        names(output.top.percent)[counter] <- names(pLCR_MFI_rep)[ grep(guide, names(pLCR_MFI_rep))] # name output
        
      }
    }
  }
}


# transform data to data frame, so ggplot can plot it
df <- melt(output.top.percent)

# Add info to df, to allow ggplot to plot it pretty nicely
GetGuideInfo <- function(x,y = 2){strsplit(as.character(x), split = "_")[[1]][y]}
GetName <- function(x,y = 1:2){paste0(strsplit(as.character(x), split = "_")[[1]][y], collapse = "")}
df$pLCR = sapply(rownames(df), FUN = GetGuideInfo, y=1)
df$Guide = sapply(rownames(df), FUN = GetGuideInfo, y=2)
df$Replicate = sapply(df$Guide, FUN = function(y){strsplit(y,split = "\\.")[[1]][2]})
#df$Sample = paste0(df$pLCR,"_",df$Guide)

# matrix with pLCR to Cas13 protein naming
ID.mat <- cbind(pLCRs,
                c("no U",rep("3 x U",4),rep("6 x U",4)),
                c(">60nt", "1nt", "18nt", "34nt","49nt", "1nt", "18nt", "34nt","49nt" ))
                
# function to retrieve Cas13 names
GetInfo <- function(x, ID = ID.mat, col = 2){
  row = which( ID[,1] == x)
  return(ID[row,col])
}

df$Ucontext = sapply(df$pLCR, FUN = GetInfo, ID = ID.mat, col=2)
df$distance = sapply(df$pLCR, FUN = GetInfo, ID = ID.mat, col=3)

df$Ucontext  = factor(df$Ucontext , levels = c( "no U",  "6 x U" ,"3 x U"))
df$distance  = factor(df$distance , levels = rev(c( ">60nt", "1nt", "18nt","34nt", "49nt" )))



set.seed(1234)
g1 = ggplot( data = df, aes( x = distance, y = value , color = Ucontext )) + 
  geom_hline( yintercept = median(df$value[grep("p130",rownames(df))]) , colour="grey", linetype = "longdash")+
  ylab("% GFP intensity rel. to ctrl") + 
  xlab("Distance to guide RNA 3'end") +
  geom_point(pch = 21, position = position_jitterdodge()) +
  coord_fixed((5/(50*3))) + 
  theme_classic() + 
  scale_color_manual(values = (c("#636363",  "#3182bd" ,"#de2d26"))) +
  scale_y_continuous(breaks=c(seq(0,50,10)), limits = c(0,50)) + 
  theme(panel.grid.major.x = element_blank()) +
  stat_summary(fun.y=mean,fun.ymin=mean, fun.ymax=mean, geom="crossbar", aes(group=Ucontext), position=position_dodge(), size=0.35)+
  geom_vline(xintercept=seq(2,5,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) 

  

df$Ucontext  = factor(df$Ucontext , levels = c( "no U","3 x U","6 x U" ))
set.seed(1234)
g2 = ggplot( data = df, aes( x = Ucontext, y = value, color = Ucontext)) + 
  geom_hline( yintercept = median(df$value[grep("p130",rownames(df))]) , colour="grey", linetype = "longdash")+
  ylab("% GFP intensity rel. to ctrl") + 
  xlab("number of upstream U") +
  geom_point(pch = 21, position = position_jitterdodge( dodge.width=1 )) +
  theme_classic() + 
  scale_color_manual(values = c("#636363","#de2d26","#3182bd" ))+
  scale_y_continuous(breaks=c(seq(0,50,10)), limits = c(0,50)) +
  theme(panel.grid.major.x = element_blank()) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.75))+
  coord_fixed((5/(30)))+
  stat_summary(fun.y=mean,fun.ymin=mean, fun.ymax=mean, geom="crossbar", aes(group=Ucontext), position=position_dodge(), size=0.35)


  
pdf("./FACS_figures/Supplementary_Note_Fig4bc.pdf", width = 10, height = 4, useDingbats = F)
grid.arrange(arrangeGrob(grobs= list(g1,g2) , ncol = 2) )
dev.off()


