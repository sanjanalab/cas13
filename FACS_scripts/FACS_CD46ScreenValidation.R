#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
# install.packages(c("EnvStats","ggplot2","reshape2","ggridges","gridExtra","grid") , lib = "~/R/x86_64-redhat-linux-gnu-library/3.5", dependencies = T)
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggridges, quietly = T))
suppressMessages(library(EnvStats, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))

Calculate_gMFI=function(a){
  m = abs(min(a))+1
  return(geoMean(a+m))}

GetInfo = function(x, INFO = info, What = 'Guide'){
  r = which(INFO$Name == x)
  return(info[r,What])
}

SelectTopCells = function(x,p = P, side = "top"){
  if (side == "top"){
    sort(x,decreasing = T)[1:round(length(x)*p)]
  }else{
    sort(x,decreasing = F)[1:round(length(x)*p)]
  }
}

CalcPercTfxpos = function(x,Q=q){
  (table(x > Q)/length(x))["TRUE"]
}


###### execute ######################################
# load data
data = read.delim( file = "./FACS_data/FACS_CD46ScreenValidation.csv", sep=",",  header = T,  stringsAsFactors = F)
info = read.delim( file = "./FACS_data/CD46ScreenValidation_Info.txt", sep="\t",  header = T,  stringsAsFactors = F)


############### Seed confirmation ########

# Note, for comparison reasons, three samples have been selected with Puromycin starting 24h post transfection till the time of the FACS experiment.
# Puromycin selection led to a small increase in the 'transfected' population size, but (when comparing the same % of transfected cells, the knock-down strength is the same)
# Conclusion made are solely based on untransfected samples.


#Plot histograms
df = melt(data[,-grep("live_dead", colnames(data))])
df$variable = as.character(df$variable)
df$Guide = sapply(as.character(df$variable) , GetInfo , INFO = info, What = 'Guide')
df$Replicate = sapply(as.character(df$variable) , GetInfo , INFO = info, What = 'Replicate')
df$Selection = sapply(as.character(df$variable) , GetInfo , INFO = info, What = 'Selection')
df$Cells = sapply(as.character(df$variable) , GetInfo , INFO = info, What = 'Cells')
df$Class = sapply(as.character(df$variable) , GetInfo , INFO = info, What = 'Class')
df$Treatment = paste0(df$Cells ,"_",df$Selection)
df$Treatment = gsub( "_FALSE" , "" , df$Treatment )
df$Treatment = gsub( "_TRUE" , "+selection" , df$Treatment )

df$Guide = factor(df$Guide , levels = c("lipo", "NT","1","2","3","4","5","6" ))
df$Class = factor(df$Class , levels = c("WT", "NT","Low","High" ))
gg1 = ggplot(df, aes(x = value+1, y = Guide)) +
  facet_wrap( ~ Treatment, ncol = 3)+
  geom_density_ridges(aes(fill = Class)) +
  scale_x_log10(limits=c(30,30000)) +
  theme_classic()+
  ggtitle("") +
  xlab("CD46 signal") +
  ylab("Guide") + 
  scale_fill_manual(values = rev(c("#3182bd","#de2d26","#bdbdbd","#f0f0f0")))


# pdf("./FACS_figures/CD46_ScreenValidation_Histogram.pdf", width = 6, height = 6, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= list(gg1) ))
# dev.off()




# determine how many cells are Transfected? (lower signal than WT control)
q = quantile(c(data[,grep("WT_lipo_1",colnames(data))],
               data[,grep("WT_lipo_2",colnames(data))],
               data[,grep("WT_lipo_3",colnames(data))],
               data[,grep("X13d_lipo_1",colnames(data))],
               data[,grep("X13d_lipo_2",colnames(data))],
               data[,grep("X13d_lipo_3",colnames(data))]), probs = c(0.025))
PercTfxpos = apply(data , 2, CalcPercTfxpos) # determine how many cells got transfected and select the same percentage across all conditions assuming equal efficiency
P = mean(PercTfxpos[grep("X13d_1|X13d_2|X13d_3",names(PercTfxpos))])
Transfected_cells = apply(data , 2, SelectTopCells, p = P , side = 'bottom')

# remove
Transfected_cells = Transfected_cells[ , -grep('live_dead',colnames(Transfected_cells)) ]
Cas13 = Transfected_cells[ , grep('X13',colnames(Transfected_cells)) ]
WT = Transfected_cells[ , grep('WT',colnames(Transfected_cells)) ]

# Calculate Mean MFI per sample
data = as.data.frame(Cas13)
data$WT = apply(data[,grep("lipo",colnames(data))], MARGIN = 1 , mean)
MFIs = colMeans(data) # colmeans to get mean mfi per column 
#MFIs = apply(data, MARGIN = 2, Calculate_gMFI) # get geometric mean (less affected by outliers)

Reference = "WT"
output.mfi.percent  <- MFIs
for (i in 1:ncol(data)){
  output.mfi.percent[i] <- (MFIs[i]/MFIs[Reference])*100
}

results.cas13 = output.mfi.percent[-length(output.mfi.percent)]



# Calculate Mean MFI per sample
data = as.data.frame(WT)
data$WT = apply(data[,grep("lipo",colnames(data))], MARGIN = 1 , mean)
MFIs = colMeans(data) # colmeans to get mean mfi per column 
#MFIs = apply(data, MARGIN = 2, Calculate_gMFI) # get geometric mean (less affected by outliers)

Reference = "WT"
output.mfi.percent  <- MFIs
for (i in 1:ncol(data)){
  output.mfi.percent[i] <- (MFIs[i]/MFIs[Reference])*100
}

results.wt = output.mfi.percent[-length(output.mfi.percent)]

output.mfi.percent = c(results.cas13, results.wt)


# transform data to data frame, so ggplot can plot it
df <- melt(output.mfi.percent)

# Add info to df, to allow ggplot to plot it pretty nicely
df$Cells = sapply(rownames(df), GetInfo , INFO = info, What = 'Cells')
df$Selection = sapply(rownames(df) , GetInfo , INFO = info, What = 'Selection')
df$Guide = sapply(rownames(df) , GetInfo , INFO = info, What = 'Guide')
df$Guide = gsub( "lipo" , "WT" , df$Guide )
df$Replicate = sapply(rownames(df) , GetInfo , INFO = info, What = 'Replicate')
df$Class = sapply(rownames(df) , GetInfo , INFO = info, What = 'Class')
df$Treatment = paste0(df$Cells ,"_",df$Selection)
df$Treatment = gsub( "_FALSE" , "" , df$Treatment )
df$Treatment = gsub( "_TRUE" , "+selection" , df$Treatment )

df$Guide = factor(df$Guide , levels = c("WT","NT","1","2","3","4","5","6"))
df$Class = factor(df$Class , levels = c("WT", "NT","Low","High" ))





g = ggplot( data = df, aes( x = Guide, y = value , color = Class )) + 
  facet_wrap( ~ Treatment, ncol = 3) +
  ylab("CD46 intensity %") + 
  xlab("guide RNA") +
  geom_point(pch = 21, position = position_jitterdodge(jitter.width = 1.5), size = 2) +
  coord_fixed((8/(110))) + 
  theme_classic() + 
  scale_color_manual(values = c("#252525","#bdbdbd","#de2d26","#3182bd")) +
  scale_y_continuous(breaks=c(seq(0,100,25)) , limits = c(0,105)) + 
  stat_summary(fun.y=mean,fun.ymin=mean, fun.ymax=mean, geom="crossbar", aes(group=Class), position=position_dodge(), size=0.35)+
  geom_vline(xintercept=seq(2,8,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) 


  pdf("./FACS_figures/FigS9c_CD46_ScreenValidation.pdf", width = 7, height = 2.5, useDingbats = F)
  grid.arrange(arrangeGrob(grobs= list(g)) )
  dev.off()

  
  