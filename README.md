# Cas13 Guide RNA Design: Supplementary code, analyses and software tools
 
This repository is intended to accompany our manuscript. For more information please refer to:
 
Hans-Hermann Wessels\*, Alejandro Méndez-Mancilla\*, Xinyi Guo, Mateusz Legut, Zharko Daniloski, Neville E. Sanjana. (2020) 
[***Massively parallel Cas13 screens reveal principles for guide RNA design***.](https://doi.org/10.1101/2019.12.27.889089) *Nature Biotechnology*, in press

The archive **Cas13design.tar.gz** entails the software needed to predict Cas13d guide RNA efficiencies on custom target RNA sequences. The software can be downloaded separately and installed following the enclosed Readme.txt file. 

```
wget -O Cas13design.tar.gz https://gitlab.com/sanjanalab/cas13/-/raw/master/Cas13designGuidePredictor/Cas13design.tar.gz?inline=false
tar -xzf Cas13design.tar.gz
```
 
For those who prefer to use pre-computed Cas13d guide RNA predictions for the human transcriptome, please see our user-friendly, web-based tool at  [https://cas13design.nygenome.org](https://cas13design.nygenome.org).


The code is released to enhance reproducibility and as a suite of complementary tools for Cas13 guide design. In addition, we hope these analyses might help others in the future who work on new datasets with novel enzymes and genome engineering tools.


If you find our code or our precomputed Cas13 guide RNA predictions to be helpful for your work, please cite the paper above.
 
 
 
To reproduce all analysis run:

```
git clone git@gitlab.com:sanjanalab/cas13.git
cd cas13
bash RunAllScripts.sh
```
 
  
   
    
    

Allthough this wrapper contains all scripts necessary to reproduce our work, it may not be advisable to run it all at once.
Instead, the wrapper is organized in modules/tasks in the order of occurance.
Each task is executed by its own script. (Some scripts may depend on data generate in preceding scripts).
Thus, if you are looking for a specific part of our analysis, you should be able to find it.






**Dependencies for running the entire pipeline (most are optional):**

*  Installation of [RNAhybrid](https://bibiserv.cebitec.uni-bielefeld.de/rnahybrid?id=rnahybrid_view_download). (Needs to be exported to your $PATH)
*  Installation of the [Vienna RNA package](https://www.tbi.univie.ac.at/RNA/#download) if needed. (RNAfold and RNAplfold binaries are provided.) 
*  Installation of [Bowtie](https://sourceforge.net/projects/bowtie-bio/files/bowtie/1.2.3/) if needed. (Needs to be exported to your $PATH)

*  Numerous R libraries listed individually in each R script, including R Bioconductor libraries. (All analysis were done using R v3.5.1)



Instructions for use:

Users are advised to read the code closely and modify commented pieces as appropriate to acquire desired output for your environment. For example, you will need to download all of the additional R library dependencies for the code to work. This being said, if you find crucial files missing, please raise a Gitlab issue and we will help out.
Please read this file first as it provides a general overview of relevant commands that were used sequentially to pre-process the data and generate the figures. This script should be able to run on the precomputed data provided in the folder to generate the figures.

Additional notes:

Our naming convention may slightly differ in the code from the one in the paper. 